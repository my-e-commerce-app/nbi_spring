package com.emil.fantasy_store_backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FantasyStoreBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(FantasyStoreBackendApplication.class, args);
	}

}
